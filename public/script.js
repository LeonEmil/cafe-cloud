const cafeList = document.getElementById("cafe-list")
const form = document.getElementById("add-cafe-form")

let fragment = document.createDocumentFragment()

const renderCafe = (doc) => {
    let li = document.createElement('li')
    let name = document.createElement('span')
    let city = document.createElement('span')
    let cross = document.createElement('div')
    
    li.setAttribute('data-id', doc.id)
    name.textContent = doc.data().name
    city.textContent = doc.data().city
    cross.textContent = 'x'
    
    li.appendChild(name)
    li.appendChild(city)
    li.appendChild(cross)
    
    fragment.appendChild(li)

    // Deleting data
    cross.addEventListener('click', (e) => {
        e.stopPropagation()
        let id = e.target.parentElement.getAttribute('data-id')
        db.collection('cafes').doc(id).delete()
    })
}

// Getting documents
// db.collection('cafes').where('city', '==', 'Moscu').orderBy('name').get()
//     .then(snapshot => {
//         snapshot.docs.forEach(doc => {
//             renderCafe(doc)
//         })
//     cafeList.appendChild(fragment)
//     })

// Saving data
form.addEventListener("submit", (e) => {
    e.preventDefault()
    db.collection('cafes').add({
        name: form.name.value,
        city: form.city.value
    })

    form.name.value = '',
    form.city.value = ''
})

// Real time snapshot
db.collection('cafes').orderBy('city').onSnapshot(snapshot => {
    let changes = snapshot.docChanges()
    changes.forEach(change => {
        console.log(change.type)
        if(change.type === "added"){
            renderCafe(change.doc)
            cafeList.appendChild(fragment)
        }
        else if(change.type === "removed"){
            let li = cafeList.querySelector(`[data-id=${change.doc.id}]`)
            cafeList.removeChild(li)
        }
        else if(change.type === "modified"){
            let li = cafeList.querySelector(`[data-id=${change.doc.id}]`)
            cafeList.removeChild(li)
            renderCafe(change.doc)
            cafeList.appendChild(fragment)
        }
    })
})

// Update and set docs
// Update actualiza los datos, set los sobreescribe

// db.collection('cafes').doc('').set({
//     name: 'Shaun cafe',
//     city: 'Liverpool'
// })